import json
from confluent_kafka import Consumer, KafkaError
import sys

c = Consumer({'bootstrap.servers': 'localhost:9092', 'group.id': '1', 'auto.offset.reset': 'earliest'})
c.subscribe(['monitor'])

while True:
	stats = c.poll(1.0)

	if stats is None:
		continue
	if stats.error():
		print("Consumer error: {}".format(stats.error()))
		continue

	data= json.loads(stats.value().decode('utf-8'))
	print('Received message: {}'.format(data))

