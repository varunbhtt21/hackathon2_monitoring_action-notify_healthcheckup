import json
from confluent_kafka import Producer
import sys, time
import psutil
import socket

server_id= 3

p = Producer({'bootstrap.servers': 'localhost:9092'})

while True:
	hostname = socket.gethostname()
	server_ip= socket.gethostbyname(hostname) 
	print (server_ip)

	cpu_utilization= psutil.cpu_percent()
	print (cpu_utilization)

	total_memory= psutil.virtual_memory().total
	print (total_memory)

	used_memory= psutil.virtual_memory().used/total_memory *100
	print (used_memory)

	free_memory= psutil.virtual_memory().available/total_memory *100
	print (free_memory)


	stats= {
		"server_id" : server_id,
		"server_ip" : server_ip,
		"cpu_utilization" : cpu_utilization,
		"used_memory" : used_memory,
		"free_memory" : free_memory
	}

	json_string= json.dumps(stats)
	p.produce('server_stats', json_string.encode('utf-8'))
	p.poll(0)

	time.sleep(30)