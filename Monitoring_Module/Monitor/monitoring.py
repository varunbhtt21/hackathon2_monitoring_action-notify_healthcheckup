from confluent_kafka import Producer
from confluent_kafka import Consumer, KafkaError
from collections import defaultdict
import json

settings = {
	'bootstrap.servers': 'localhost:9092',
	'group.id': 'mygroup',
	'client.id': 'client-1',
	'enable.auto.commit': True,
	'session.timeout.ms': 6000,
	'default.topic.config': {'auto.offset.reset': 'smallest'}
}

def Produce_To_LoadBalancer():
	Overall_Stat = json.dumps(Overall_Stats)
	p.produce('monitor', Overall_Stat.encode('utf-8'))
	p.poll(0)

def Update_Stats(stats):
	decoded_stats = json.loads(stats)

	for key, value in decoded_stats.items():
		Overall_Stats[decoded_stats["server_id"]][key] = value

	print(Overall_Stats)

	Produce_To_LoadBalancer()



def Consume_From_Server():
	while True: 
		while 1:
			msg = c.poll(0.1)
			if msg is None:
				continue
			elif not msg.error():
				stats = msg.value().decode("utf-8")
				break  

		Update_Stats(stats)


c = Consumer(settings)
c.subscribe(['server_stats'])
p = Producer({'bootstrap.servers': 'localhost:9092'})
Overall_Stats = defaultdict(dict)

Consume_From_Server()