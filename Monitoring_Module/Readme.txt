**Steps to Run**

1. Start Kafka Server and Kafka Zookeeper.
2. Execute Monitoring Module as-
		python3 monitoring.py
3. Execute Runtime Server as-
		python3 server.py

**Outputs**

Monitoring module recieves a JSON string as an input from every Runtime Server which the monitoring module combines into one dictionary and produces it to the Load Balancer to check all the server stats.


**JSON Format**

1. Received JSON string at Monitoring Module as- 
{
	'server_id': server_id
	'server_ip': server_ip
	'cpu_utilization' : cpu_utilization
	'used_memory' : used_memory
	'free_memory' : free_memory
}

2. Received JSON string at Load Balancer as-
{
	'server_id':
	{
		'server_id': server_id
		'server_ip': server_ip
		'cpu_utilization' : cpu_utilization
		'used_memory' : used_memory
		'free_memory' : free_memory
	}

	'server_id':
	{
		'server_id': server_id
		'server_ip': server_ip
		'cpu_utilization' : cpu_utilization
		'used_memory' : used_memory
		'free_memory' : free_memory
	}
	.
	.
	.
}
