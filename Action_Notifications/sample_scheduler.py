from confluent_kafka import Producer
import json

data= {
	"user_id" : 'A',
	"server_id" : '1',
	"server_ip" : '192.168.43.228',
	"start_time" : '12:23',
	"status" : 'yes',
	"email_id" : "varunbhatt21@gmail.com"
}

p = Producer({'bootstrap.servers': 'localhost:9092'})
while True:

	json_string= json.dumps(data)
	p.produce('action_notify_scheduler', json_string.encode('utf-8'))
	p.poll(0)


	data= {
		"user_id" : 'B',
		"server_id" : '2',
		"server_ip" : '192.168.43.128',
		"start_time" : '12:23',
		"status" : "yes",
		"email_id" : "smriti.swtsmi@gmail.com"
	}
	
	# break