from confluent_kafka import Producer
import json

data= {
    "user_id" : 'A',
    "server_id" : '1',
    "server_ip" : '192.168.43.228',
	"status" : "yes",
	"email_id" : "varunbhatt21@gmail.com"
}

p = Producer({'bootstrap.servers': 'localhost:9092'})

count= 0
while True:
	# count+=1
	
	json_string= json.dumps(data)
	p.produce('action_notify_deployer', json_string.encode('utf-8'))
	p.poll(2)

	data= {
		"user_id" : 'B',
		"server_id" : '2',
		"server_ip" : '192.168.43.128',
		"status" : "yes",
		"email_id" : "smriti.swtsmi@gmail.com"
	}
	
	# if(count ==2):
	# 	break
	# break