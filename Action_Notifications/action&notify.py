import threading
from confluent_kafka import Producer, Consumer
import smtplib 
import json
import time



sender_email = "varunsurat1995@gmail.com"


# define an Dictionary
user_info= {}

settings = {
	'bootstrap.servers': 'localhost:9092',
	'group.id': 'mygroup',
	'client.id': 'client-1',
	'enable.auto.commit': True,
	'session.timeout.ms': 6000,
	'default.topic.config': {'auto.offset.reset': 'smallest'}
}

c = Consumer(settings)
c.subscribe(['action_notify_deployer'])

Sch = Consumer(settings)
Sch.subscribe(['action_notify_scheduler'])

Ser = Consumer(settings)
Ser.subscribe(['action_notify_server'])


def Notify_Deploy(stats):

	email = smtplib.SMTP('smtp.gmail.com', 587) 
	email.starttls() 
	email.login("varunsurat1995@gmail.com", "Varun@123") 

	Notify_Info = json.loads(stats)
	print("Notification Msg ",Notify_Info)

	receiver_email = Notify_Info["email_id"]

	if Notify_Info["status"] == "yes":
		Status = "Your Application has been successfully Deployed."
	else:
		Status = "Your Application has been failed to Deploy."

	message = """\

	Subject : Deployment Notification

	User_ID = """ + Notify_Info["user_id"] + """
	Server IP = """ + Notify_Info["server_ip"] + """
	Status = """ + Status
  
	# terminating the session 
	email.sendmail(sender_email, receiver_email, message)
	email.quit()
	print("Deploy Done")


def Notify_Scheduler(stats):

	email = smtplib.SMTP('smtp.gmail.com', 587) 
	email.starttls() 
	email.login("varunsurat1995@gmail.com", "Varun@123") 

	Notify_Info = json.loads(stats)
	print("Notification Msg ",Notify_Info)

	receiver_email = Notify_Info["email_id"]

	if Notify_Info["status"] == "yes":
		Status = "Your Application has been successfully Scheduled at "+Notify_Info["start_time"]
	else:
		Status = "Your Application has been failed to Scheduled at "+Notify_Info["start_time"]

	message = """\

	Subject : Scheduling Notification

	User_ID = """ + Notify_Info["user_id"] + """
	Server IP = """ + Notify_Info["server_ip"] + """
	Status = """ + Status
  
	# terminating the session 
	email.sendmail(sender_email, receiver_email, message)
	email.quit()
	print("Schedule Done")



def Notify_Server(stats):

	email = smtplib.SMTP('smtp.gmail.com', 587) 
	email.starttls() 
	email.login("varunsurat1995@gmail.com", "Varun@123") 

	Notify_Info = json.loads(stats)
	print("Notification Msg ",Notify_Info)

	receiver_email = Notify_Info["email_id"]

	
	message = """\

	Subject : Interaction Notification

	User_ID = """ + Notify_Info["user_id"] + """
	Status = """ + Notify_Info["status"] 
  
	# terminating the session 
	email.sendmail(sender_email, receiver_email, message)
	email.quit()
	print("Server Done")

	


def Consume_From_Scheduler():	

	while True: 
		msg = Sch.poll(0.1)

		if msg is None:
				continue	

		while 1:
			if not msg.error():
				stats = msg.value().decode("utf-8")
				break
				
		if stats not in user_info:
			user_info[stats] = 1
			Notify_Scheduler(stats)


def Consume_From_Deployer():	
	
	while True: 
		msg = c.poll(0.1)


		if msg is None:
				continue	
		while 1:
			
			if not msg.error():
				stats = msg.value().decode("utf-8")
				
				break
				
		if stats not in user_info:
			user_info[stats] = 1
			Notify_Deploy(stats)
		
		


def Consume_From_Server():	

	while True: 
		msg = Ser.poll(0.1)

		if msg is None:
				continue	

		while 1:
			if not msg.error():
				stats = msg.value().decode("utf-8")
				break
				
		if stats not in user_info:
			user_info[stats] = 1
			Notify_Server(stats)




scheduller_thread= threading.Thread(target= Consume_From_Scheduler)
deployer_thread= threading.Thread(target= Consume_From_Deployer)
server_thread= threading.Thread(target= Consume_From_Server)

deployer_thread.start()
scheduller_thread.start()
server_thread.start()

