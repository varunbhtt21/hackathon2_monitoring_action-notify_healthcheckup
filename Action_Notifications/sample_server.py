from confluent_kafka import Producer
import json

p = Producer({'bootstrap.servers': 'localhost:9092'})
def display(message):
    data= {
        "user_id" : 'A',
        "status" : message,
        "email_id" : "varunbhatt21@gmail.com"
    }

    while True:
        json_string= json.dumps(data)
        p.produce('action_notify_server', json_string.encode('utf-8'))
        p.poll(0)


display("The temperature is too low..you might shiver:p")