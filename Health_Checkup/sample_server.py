# !/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

class MyHandler(BaseHTTPRequestHandler):

    def do_GET(self):

        message = "Alive from Server"
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        self.wfile.write(bytes(message, "utf8"))
        time.sleep(5) 
        return


def main():

    print('starting server on port 8082...')

    server_address = ('127.0.0.1', 8082)
    while True:
        httpd = HTTPServer(server_address, MyHandler)
        httpd.serve_forever()
        

main()
