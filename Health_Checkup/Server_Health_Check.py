#!/usr/bin/env python3

import requests as req
import threading

def deployer_health():
    headers = {'user-health_deployer': ''}

    while True:
        try:
            response = req.get("http://localhost:8081/health_deployer", headers=headers)
            print(response.text)
        except:
            print ("ERROR")
            break

def server_health():
    headers = {'user-health_server': ''}

    while True:
        try:
            response = req.get("http://localhost:8082/health_server", headers=headers)
            print(response.text)
        except:
            print ("ERROR")
            break

def scheduler_health():
    headers = {'user-health_scheduler': ''}

    while True:
        try:
            response = req.get("http://localhost:8083/health_scheduler", headers=headers)
            print(response.text)
        except:
            print ("ERROR")
            break



deployer_thread= threading.Thread(target=deployer_health)
server_thread= threading.Thread(target=server_health)
scheduler_thread= threading.Thread(target=scheduler_health)

deployer_thread.start()
server_thread.start()
scheduler_thread.start()